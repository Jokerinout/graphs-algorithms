class Main {
    public static void main (String[] args){
        
        Sommet s1 = new Sommet("1");
        Sommet s2 = new Sommet("2");
        Sommet s3 = new Sommet("3");
        Sommet s4 = new Sommet("4");
        Sommet s5 = new Sommet("5");
        Sommet s6 = new Sommet("6");
        Sommet s7 = new Sommet("7");
        Sommet s8 = new Sommet("8");
        Sommet s9 = new Sommet("9");

        s1.addVoisin(s4,8);

        s2.addVoisin(s1,7);

        s3.addVoisin(s2,2);
        s3.addVoisin(s6,3);
        s3.addVoisin(s1,1);

        s4.addVoisin(s3,2);
        s4.addVoisin(s7,5);

        s5.addVoisin(s6,6);
        s5.addVoisin(s8,5);
        s5.addVoisin(s2,9);

        s6.addVoisin(s7,3);

        s7.addVoisin(s9,7);

        s8.addVoisin(s9,8);
        s8.addVoisin(s6,1);

        s9.addVoisin(s4,10);

        Graphe g = new Graphe();

        g.ajouter(s1);
        g.ajouter(s2);
        g.ajouter(s3);
        g.ajouter(s4);
        g.ajouter(s5);
        g.ajouter(s6);
        g.ajouter(s7);
        g.ajouter(s8);
        g.ajouter(s9);

//--------------------------------------------------------------------------------

        Sommet s10 = new Sommet("10");
        Sommet s11 = new Sommet("11");
        Sommet s12 = new Sommet("12");
        Sommet s13 = new Sommet("13");

        s10.addVoisin(s11,8);
        s10.addVoisin(s12,8);
        s11.addVoisin(s13,8);
        s12.addVoisin(s10,8);
        s13.addVoisin(s11,8);

        Graphe g2 = new Graphe();

        g2.ajouter(s10);
        g2.ajouter(s11);
        g2.ajouter(s12);
        g2.ajouter(s13);

//--------------------------------------------------------------------------------

        g.afficher();

        System.out.println();

        g.transposee().afficher();

        System.out.println();

        System.out.println(g.plus_court_chemin_Dijkstra(s1));

        System.out.println(g.checkBipartit());
        System.out.println(g2.checkBipartit());
    }
}
