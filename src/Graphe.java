import java.util.*;

public class Graphe {
    private List<Sommet> liste;

    public Graphe() {
        this.liste = new LinkedList<>();
    }

    public List<Sommet> getListe() {
        return liste;
    }

    public void ajouter(Sommet S) {
        this.liste.add(S);
    }

    public void remove(Sommet S) {
        int i = 0;
        boolean b = false;
        while(i < this.liste.size() && b == false) {
            if(this.liste.get(i) == S) {
                this.liste.remove(i);
                b = true;
            }
        }
    }

    public Integer find(Sommet S) {
        for(int i=0; i<this.liste.size(); i++) {
            if(this.liste.get(i) == S) {
                return i;
            }
        }
        return -1;
    }

    public Integer getCout(Sommet s1, Sommet s2) {
        return s1.coutVoisin(s2);
    }

    public Graphe transposee() {
        Graphe g = new Graphe();
        for(Sommet s : liste) {
            Sommet som = new Sommet(s.getNom());
            g.ajouter(som);
        }
        for(int i=0; i<this.liste.size(); i++) {
            for(Sommet sommet : this.liste.get(i).getVoisins()) {
                g.getSommetByName(sommet.getNom()).addVoisin(this.liste.get(i),this.getCout(this.liste.get(i),sommet));
            }
        }
        return g;
    }

    public Sommet getSommetByName(String name) {
        for(int i=0; i<this.liste.size(); i++) {
            if(this.liste.get(i).getNom().equals(name)) {
                return this.liste.get(i);
            }
        }
        return null;
    }

    public List<String> plus_court_chemin_Dijkstra(Sommet dep) {
      List<String> temp = new ArrayList<String>();
      Sommet selected = null;
      for(Sommet s : liste) {
          if(s == dep) {
            temp.add("0/"+dep.getNom());
            selected = s;
            s.setMarque();
          }else {
            temp.add("inf/"+dep.getNom());
          }
      }
      recursiveDijkstra(temp,selected);
      return temp;
    }

    private List<String> recursiveDijkstra(List<String> tab, Sommet s) {
        Sommet selected = null;
        for(int i=0;i<s.getVoisins().size(); i++) {
          if(!s.getVoisins().get(i).isMarque()) {
              if(tab.get(find(s.getVoisins().get(i))).split("/")[0].equals("inf") || s.getCouts().get(i)+Integer.parseInt(tab.get(find(s)).split("/")[0]) <= Integer.parseInt(tab.get(find(s.getVoisins().get(i))).split("/")[0])) {
                String value = (s.getCouts().get(i)+Integer.parseInt(tab.get(find(s)).split("/")[0])) + "/" + s.getNom();
                tab.set(find(s.getVoisins().get(i)),value);
              }
            }
          }
          selected = get_min(tab);
          if(selected != null) {
              selected.setMarque();
              recursiveDijkstra(tab,selected);
          }
          return tab;
    }

    private Sommet get_min(List<String> tab) {
        Sommet selected = null;
        int min = -1;
        for(int i=0;i<tab.size();i++) {
            if(!liste.get(i).isMarque() && !tab.get(i).split("/")[0].equals("inf")) {
                if(min == -1 || min > Integer.parseInt(tab.get(i).split("/")[0])) {
                    min = Integer.parseInt(tab.get(i).split("/")[0]);
                    selected = liste.get(i);
                }
            }
        }
        return selected;
    }

    public boolean checkBipartit() {
        List<String> ens1 = new ArrayList<String>();
        List<String> ens2 = new ArrayList<String>();
        for(int i=0;i<this.liste.size();i++) {
            if(ens1.size() == 0) {
                ens1.add(this.liste.get(i).getNom());
                for(Sommet som : this.liste.get(i).getVoisins()) {
                    ens2.add(som.getNom());
                }
            } else {
                if(ens1.contains(this.liste.get(i).getNom())) {
                    for(Sommet som : this.liste.get(i).getVoisins()) {
                        if(ens1.contains(som.getNom())) {
                            return false;
                        } else {
                            if(!ens2.contains(som.getNom())) {
                                ens2.add(som.getNom());
                            }
                        }
                    }
                } else {
                    if(ens2.contains(this.liste.get(i).getNom())) {
                        for(Sommet som : this.liste.get(i).getVoisins()) {
                            if(ens2.contains(som.getNom())) {
                                return false;
                            } else {
                                if(!ens1.contains(som.getNom())) {
                                    ens1.add(som.getNom());
                                }
                            }
                        }
                    } else {
                        ens1.add(this.liste.get(i).getNom());
                        for(Sommet som : this.liste.get(i).getVoisins()) {
                            ens2.add(som.getNom());
                        }
                    }
                }
            }
        }
        return true;
    }

    public void afficher() {
        for(Sommet s : liste) {
            s.afficher();
            System.out.println();
        }
    }

}
