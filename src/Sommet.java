import java.util.LinkedList;
import java.util.List;

public class Sommet {
    private String nom;
    private boolean marque = false;
    private List<Sommet> voisins;
    private List<Integer> couts;

    public Sommet(String name){
        this.nom = name;
        this.voisins = new LinkedList<>();
        this.couts = new LinkedList<>();
    }

    public String getNom() {
        return nom;
    }

    public boolean isMarque() {
        return marque;
    }

    public List<Sommet> getVoisins() {
        return voisins;
    }

    public List<Integer> getCouts() {
        return couts;
    }

    public Integer coutVoisin(Sommet S) {
        for(int i=0; i<this.voisins.size();i++){
            if(this.voisins.get(i) == S) {
                return this.couts.get(i);
            }
        }
        return -1;
    }

    public void setMarque() {
        if(this.marque == true) {
            this.marque = false;
        }else{
            this.marque = true;
        }
    }

    public void addVoisin(Sommet s, Integer c) {
        this.voisins.add(s);
        this.couts.add(c);
    }

    public void removeVoisin(Sommet S) {
        int i = 0;
        boolean b = false;
        while(i < this.voisins.size() && b == false) {
            if(this.voisins.get(i) == S) {
                this.voisins.remove(i);
                this.couts.remove(i);
                b = true;
            }
        }
    }

    public void afficher() {
        System.out.print(getNom()+" : ");
        for(Sommet s : voisins) {
            System.out.print(s.getNom()+" ");
        }
    }
}
